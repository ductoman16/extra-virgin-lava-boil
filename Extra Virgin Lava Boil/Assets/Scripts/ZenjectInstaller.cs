﻿namespace Assets.Scripts
{
    using Assets.Scripts.Core;

    using Zenject;

    public class ZenjectInstaller : MonoInstaller<ZenjectInstaller>
    {
        public override void InstallBindings()
        {
            var levelConfig = new LevelConfig();
            levelConfig.SetLevel(LevelCounter.LevelNumber);

            Container.Bind<LevelConfig>().FromInstance(levelConfig);
            Container.Bind<GameStateManager>().AsSingle();
        }
    }
}
