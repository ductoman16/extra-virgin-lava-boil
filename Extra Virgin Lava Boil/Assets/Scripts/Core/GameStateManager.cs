﻿namespace Assets.Scripts.Core
{
    using System;

    using Stateless;

    public enum GameState { NotStarted, Active, Paused, Lost, Won }

    public enum GameStateEvent { Pause, Start, Win, Lose, Reset }

    public class GameStateManager
    {
        private readonly StateMachine<GameState, GameStateEvent> _machine;

        public GameStateManager()
        {
            _machine = new StateMachine<GameState, GameStateEvent>(() => GameState, g => GameState = g);

            _machine.Configure(GameState.Paused)
                .Permit(GameStateEvent.Pause, GameState.Active);

            _machine.Configure(GameState.Active)
                .Permit(GameStateEvent.Pause, GameState.Paused)
                .Permit(GameStateEvent.Win, GameState.Won)
                .Permit(GameStateEvent.Lose, GameState.Lost);

            _machine.Configure(GameState.Won)
                .Permit(GameStateEvent.Reset, GameState.NotStarted)
                .OnEntry(() => OnWin?.Invoke());

            _machine.Configure(GameState.Lost)
                .Permit(GameStateEvent.Reset, GameState.NotStarted)
                .OnEntry(() => OnLose?.Invoke());

            _machine.Configure(GameState.NotStarted)
                .Permit(GameStateEvent.Start, GameState.Active);
        }

        public event Action OnWin;

        public event Action OnLose;

        public void OnTriggered(GameStateEvent @event, Action action)
        {
            _machine.OnTransitioned(
                transition =>
                    {
                        if (transition.Trigger == @event)
                        {
                            action?.Invoke();
                        }
                    });
        }

        public GameState GameState { get; private set; } = GameState.NotStarted;

        public void Trigger(GameStateEvent @event, bool skipIfNotValid = false)
        {
            if (skipIfNotValid)
            {
                if (_machine.CanFire(@event))
                {
                    _machine.Fire(@event);
                }
            }
            else
            {
                _machine.Fire(@event);
            }
        }
    }
}
