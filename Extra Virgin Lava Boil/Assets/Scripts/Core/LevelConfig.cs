﻿namespace Assets.Scripts.Core
{
    public class LevelConfig
    {
        public int SacrificesRequired = 1;
        public int TimerSeconds = 60;
        public float FireballSpawnRate = 5;
        public int VirginsToSpawn = 3;
        public int ChanceBoxesToSpawn = 6;
        public float ChanceBoxSuccessPercent = 70;

        private const int MinTimerSeconds = 45;

        public void SetLevel(int levelNumber)
        {
            SacrificesRequired = levelNumber;
            VirginsToSpawn = SacrificesRequired * 2;
            FireballSpawnRate = 5 + levelNumber;
            //TimerSeconds = Math.Max(MinTimerSeconds, 60 - (2 * levelNumber));
        }
    }
}
