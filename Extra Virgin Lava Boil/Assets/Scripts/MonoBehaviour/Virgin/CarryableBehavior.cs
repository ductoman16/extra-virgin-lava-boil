﻿namespace Assets.Scripts.MonoBehaviour.Virgin
{
    using UnityEngine;

    public class CarryableBehavior : MonoBehaviour
    {
        public Rigidbody2D Rigidbody { get; private set; }

        public void Start()
        {
            Rigidbody = GetComponent<Rigidbody2D>();
        }

        public void Update()
        {

        }
    }
}
