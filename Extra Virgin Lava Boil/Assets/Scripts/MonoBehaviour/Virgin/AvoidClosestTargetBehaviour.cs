﻿namespace Assets.Scripts.MonoBehaviour.Virgin
{
    using System.Linq;

    using UnityEngine;

    public class AvoidClosestTargetBehaviour : MonoBehaviour
    {
        private Rigidbody2D _rigidBody;

        public Transform[] Targets { get; set; }

        private void Start()
        {
            _rigidBody = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            var distances = Targets.ToDictionary(t => (t.position - transform.position).magnitude, t => t);
            var smallestDistance = Mathf.Min(distances.Select(t => t.Key).ToArray());

            var closestTarget = distances[smallestDistance];

            var towardsTarget = closestTarget.position - transform.position;
            var awayFromTarget = towardsTarget * -1;
            var normalized = awayFromTarget.normalized;
            _rigidBody.velocity = new Vector2(normalized.x, normalized.y);
        }
    }
}
