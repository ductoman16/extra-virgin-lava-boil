﻿namespace Assets.Scripts.MonoBehaviour.Virgin
{
    using Assets.Scripts.Core;

    using UnityEngine;

    using Zenject;

    public class VirginSpawner : MonoBehaviour
    {
        public GameObject VirginPrefab;
        public Collider2D SpawnArea;

        public Transform[] AvoidTargets;
        public Collider2D GameBoundary;

        private int _virginsToSpawn;
        private GameStateManager _gameStateManager;

        [Inject]
        public void Inject(GameStateManager gameStateManager, LevelConfig config)
        {
            _virginsToSpawn = config.VirginsToSpawn;
            _gameStateManager = gameStateManager;
        }

        // Use this for initialization
        private void Start()
        {
            _gameStateManager.OnTriggered(GameStateEvent.Start, Spawn);
        }

        private void Spawn()
        {
            var spawnAreaBounds = SpawnArea.bounds;

            for (int i = 0; i < _virginsToSpawn; i++)
            {
                var virgin = Instantiate(VirginPrefab);

                var x = Random.Range(spawnAreaBounds.min.x, spawnAreaBounds.max.x);
                var y = Random.Range(spawnAreaBounds.min.y, spawnAreaBounds.max.y);
                virgin.transform.position = new Vector2(x, y);

                virgin.GetComponent<StayInBounds>().Boundary = GameBoundary;
                virgin.GetComponent<AvoidClosestTargetBehaviour>().Targets = AvoidTargets;
            }
        }
    }
}
