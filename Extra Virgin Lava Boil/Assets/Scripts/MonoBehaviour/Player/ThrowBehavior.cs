﻿namespace Assets.Scripts.MonoBehaviour.Player
{
    using Assets.Scripts;
    using Assets.Scripts.MonoBehaviour.Game;

    using UnityEngine;

    public class ThrowBehavior : MonoBehaviour
    {
        public GameObject ThrowingArea;
        public Transform ThrowTarget;
        public float MaxThrowAcceleration = 20;
        public AudioClip ThrowSound;

        public ObjectiveController ObjectiveController;

        private PickUpBehavior _pickUpBehavior;

        public void Start()
        {
            _pickUpBehavior = GetComponent<PickUpBehavior>();
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            var inThrowableArea = ReferenceEquals(collision.gameObject, ThrowingArea);
            var canThrow = inThrowableArea && _pickUpBehavior.Holding != null;

            if (canThrow && Input.GetButtonDown(Inputs.Action))
            {
                Throw();
            }
        }

        private void Throw()
        {
            var holding = _pickUpBehavior.Holding;
            //holding.Rigidbody.velocity = Vector2.MoveTowards(holding.Rigidbody.velocity, ThrowTarget.position, MaxThrowAcceleration);
            _pickUpBehavior.Drop();
            Destroy(holding.gameObject);
            ObjectiveController.SacrificesMade += 1;

            var audioSource = GetComponent<AudioSource>();
            audioSource.clip = ThrowSound;
            audioSource.Play();
        }
    }
}
