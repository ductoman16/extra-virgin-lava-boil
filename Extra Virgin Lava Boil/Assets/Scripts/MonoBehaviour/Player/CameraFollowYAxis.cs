﻿namespace Assets.Scripts.MonoBehaviour.Player
{
    using UnityEngine;

    public class CameraFollowYAxis : MonoBehaviour
    {
        public Transform Target;

        private Camera _camera;

        public void Start()
        {
            _camera = GetComponent<Camera>();
        }

        public void Update()
        {
            var currentPos = _camera.transform.position;
            var newY = Target.transform.position.y;
            _camera.transform.position = new Vector3(currentPos.x, newY, currentPos.z);
        }
    }
}
