﻿namespace Assets.Scripts.MonoBehaviour.Player
{
    using System.Collections.Generic;

    using UnityEngine;

    public class CollectChickens : MonoBehaviour
    {
        public Queue<GameObject> Chickens { get; } = new Queue<GameObject>();

        public void DropChicken()
        {
            var chicken = Chickens.Dequeue();
            Destroy(chicken);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (IsChicken(other) && !Chickens.Contains(other.gameObject))
            {
                CollectChicken(other);
            }
        }

        private void CollectChicken(Collider2D other)
        {
            Chickens.Enqueue(other.gameObject);
            var trailBehindTarget = other.GetComponent<FollowBehindTarget>();
            trailBehindTarget.Target = transform;
        }

        private bool IsChicken(Collider2D other)
        {
            return other.tag == "Chicken";
        }
    }
}
