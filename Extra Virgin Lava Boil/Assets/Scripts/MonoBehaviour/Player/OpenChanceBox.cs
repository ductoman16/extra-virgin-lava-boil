﻿namespace Assets.Scripts.MonoBehaviour.Player
{
    using Assets.Scripts.MonoBehaviour.ChanceBox;

    using UnityEngine;

    public class OpenChanceBox : MonoBehaviour
    {
        private void OnTriggerStay2D(Collider2D other)
        {
            var chanceBoxStorage = other.GetComponent<ChanceBoxStorage>();
            var isChanceBox = chanceBoxStorage != null;

            if (isChanceBox && Input.GetButton(Inputs.Action))
            {
                chanceBoxStorage.Open();
            }
        }
    }
}
