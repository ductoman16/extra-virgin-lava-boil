﻿namespace Assets.Scripts.MonoBehaviour.Player
{
    using Assets.Scripts.MonoBehaviour.Virgin;

    using UnityEngine;

    public class PickUpBehavior : MonoBehaviour
    {
        public float PickUpPositionOffset = .05f;
        public AudioClip PickUpSound;
        public AudioClip DropSound;

        private Transform _thisTransform;

        public CarryableBehavior Holding { get; private set; }

        public void Drop()
        {
            if (Holding != null)
            {
                Holding.GetComponent<StayInBounds>().enabled = true;
                Holding.GetComponent<BoxCollider2D>().enabled = true;
                Holding.GetComponent<AvoidClosestTargetBehaviour>().enabled = true;
                Holding = null;
            }
        }

        private void Start()
        {
            _thisTransform = GetComponent<Transform>();
        }

        private void FixedUpdate()
        {
            if (Holding != null)
            {
                Holding.Rigidbody.position = new Vector2(
                    _thisTransform.position.x + PickUpPositionOffset,
                    _thisTransform.position.y - PickUpPositionOffset);
            }
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            var carryable = collision.GetComponent<CarryableBehavior>();
            var canCarry = Holding == null && carryable != null;
            if (canCarry && Input.GetButtonUp(Inputs.Action))
            {
                PickUp(carryable);
            }
        }

        private void PickUp(CarryableBehavior carryable)
        {
            Holding = carryable;

            carryable.GetComponent<StayInBounds>().enabled = false;
            carryable.GetComponent<AvoidClosestTargetBehaviour>().enabled = false;
            Holding.GetComponent<BoxCollider2D>().enabled = false;

            var otherTransform = carryable.GetComponent<Transform>();
            otherTransform.position = new Vector3(
                otherTransform.position.x,
                otherTransform.position.y,
                otherTransform.position.z - 2);

            var audioSource = GetComponent<AudioSource>();
            audioSource.clip = PickUpSound;
            audioSource.Play();
        }
    }
}
