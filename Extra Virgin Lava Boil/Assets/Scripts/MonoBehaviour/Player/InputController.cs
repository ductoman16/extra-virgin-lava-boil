﻿namespace Assets.Scripts.MonoBehaviour.Player
{
    using Assets.Scripts.Core;

    using UnityEngine;

    using Zenject;

    public class InputController : MonoBehaviour
    {
        public float SpeedMultiplier = 1;

        private Rigidbody2D _rigidBody;
        private GameStateManager _gameStateManager;

        [Inject]
        public void Inject(GameStateManager gameStateManager)
        {
            _gameStateManager = gameStateManager;
        }

        // Use this for initialization
        private void Start()
        {
            _rigidBody = GetComponent<Rigidbody2D>();
        }

        // Update is called once per frame
        private void Update()
        {
            HandleMovement();

            if (Input.GetButtonDown(Inputs.Pause))
            {
                _gameStateManager.Trigger(GameStateEvent.Pause, true);
            }
        }

        private void HandleMovement()
        {
            var horizontal = Input.GetAxisRaw("Horizontal");
            var vertical = Input.GetAxisRaw("Vertical");

            Move(horizontal, vertical);
        }

        private void Move(float horizontal, float vertical)
        {
            _rigidBody.velocity = new Vector2(horizontal * SpeedMultiplier, vertical * SpeedMultiplier);
        }
    }
}
