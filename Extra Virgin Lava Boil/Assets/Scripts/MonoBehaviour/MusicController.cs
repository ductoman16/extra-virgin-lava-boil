﻿namespace Assets.Scripts.MonoBehaviour
{
    using Assets.Scripts.Core;

    using UnityEngine;

    using Zenject;

    public class MusicController : MonoBehaviour
    {
        public AudioClip[] Music;

        private GameStateManager _gameStateManager;

        private AudioSource _audioSource;

        [Inject]
        public void Inject(GameStateManager gameStateManager)
        {
            _gameStateManager = gameStateManager;
        }

        private void Start()
        {
            _audioSource = GetComponent<AudioSource>();

            _gameStateManager.OnTriggered(GameStateEvent.Start, PlayRandomMusic);
        }

        private void StopMusic()
        {
            _audioSource.Stop();
        }

        private void PlayRandomMusic()
        {
            int randClip = Random.Range(0, Music.Length);
            _audioSource.clip = Music[randClip];
            _audioSource.Play();
        }
    }
}
