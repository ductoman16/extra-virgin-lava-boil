﻿namespace Assets.Scripts.MonoBehaviour.ChanceBox
{
    using UnityEngine;

    public class StunPlayer : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            var canBeStunnedComponent = other.GetComponent<CanBeStunned>();
            if (canBeStunnedComponent != null)
            {
                canBeStunnedComponent.GetStunned();
                Destroy(gameObject);
            }
        }
    }
}
