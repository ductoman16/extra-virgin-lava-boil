﻿namespace Assets.Scripts.MonoBehaviour.ChanceBox
{
    using Assets.Scripts.Core;

    using UnityEngine;

    using Zenject;

    public class ChanceBoxSpawner : MonoBehaviour
    {
        public GameObject ChanceBoxPrefab;
        public Collider2D SpawnArea;
        public GameObject GoodContentsPrefab;
        public GameObject BadContentsPrefab;
        public Transform Player;

        private int _chanceBoxesToSpawn;
        private float _goodContentsSpawnPercent;
        private GameStateManager _gameStateManager;

        [Inject]
        public void Inject(GameStateManager gameStateManager, LevelConfig config)
        {
            _chanceBoxesToSpawn = config.ChanceBoxesToSpawn;
            _goodContentsSpawnPercent = config.ChanceBoxSuccessPercent;
            _gameStateManager = gameStateManager;
        }

        private void Start()
        {
            _gameStateManager.OnTriggered(GameStateEvent.Start, Spawn);
        }

        private void Spawn()
        {
            var spawnAreaBounds = SpawnArea.bounds;

            for (int i = 0; i < _chanceBoxesToSpawn; i++)
            {
                SpawnChanceBox(spawnAreaBounds);
            }
        }

        private void SpawnChanceBox(Bounds spawnAreaBounds)
        {
            var chanceBox = Instantiate(ChanceBoxPrefab);

            SetPosition(spawnAreaBounds, chanceBox);
            SetContents(chanceBox);
        }

        private void SetPosition(Bounds spawnAreaBounds, GameObject chanceBox)
        {
            var x = Random.Range(spawnAreaBounds.min.x, spawnAreaBounds.max.x);
            var y = Random.Range(spawnAreaBounds.min.y, spawnAreaBounds.max.y);
            chanceBox.transform.position = new Vector2(x, y);
        }

        private void SetContents(GameObject chanceBox)
        {
            var chanceBoxStorage = chanceBox.GetComponent<ChanceBoxStorage>();

            var goodOrBad = Random.Range(0, 100);
            if (goodOrBad < _goodContentsSpawnPercent)
            {
                chanceBoxStorage.SetContents(GoodContentsPrefab, g => { });
            }
            else
            {
                chanceBoxStorage.SetContents(
                    BadContentsPrefab,
                    g =>
                        {
                            g.GetComponent<FollowBehindTarget>().Target = Player;
                        });
            }
        }
    }
}
