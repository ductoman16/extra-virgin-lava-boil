﻿namespace Assets.Scripts.MonoBehaviour.ChanceBox
{
    using Assets.Scripts.MonoBehaviour.Player;
    using UnityEngine;

    public class CanBeStunned : MonoBehaviour
    {
        public float StunTime = 2;
        public AudioClip StunSound;

        private readonly Color _stunColor = Color.blue;

        private float _stunStart;
        private InputController _inputController;
        private Rigidbody2D _rigidBody;
        private SpriteRenderer _sprite;
        private Color _defaultSpriteColor;
        private RigidbodyConstraints2D _defaultConstraints;

        public void GetStunned()
        {
            _stunStart = Time.time;
            _inputController.enabled = false;
            _rigidBody.constraints = RigidbodyConstraints2D.FreezeAll;

            var audioSource = GetComponent<AudioSource>();
            audioSource.clip = StunSound;
            audioSource.Play();
        }

        private void Start()
        {
            _inputController = GetComponent<InputController>();
            _rigidBody = GetComponent<Rigidbody2D>();
            _sprite = GetComponent<SpriteRenderer>();
            _defaultSpriteColor = _sprite.color;
            _defaultConstraints = _rigidBody.constraints;
        }

        private void FixedUpdate()
        {
            if (_stunStart > 0)
            {
                if (Time.time > _stunStart + StunTime)
                {
                    UnStun();
                }
                else
                {
                    var timeStamp = (int)Mathf.Round(Time.time * 10);
                    var isEven = timeStamp % 2 == 0;
                    _sprite.color = isEven ? _stunColor : _defaultSpriteColor;
                }
            }
        }

        private void UnStun()
        {
            _stunStart = 0;
            _inputController.enabled = true;
            _rigidBody.constraints = _defaultConstraints;
            _sprite.color = _defaultSpriteColor;
        }
    }
}