﻿namespace Assets.Scripts.MonoBehaviour.ChanceBox
{
    using System;

    using UnityEngine;

    public class ChanceBoxStorage : MonoBehaviour
    {
        private bool _opened;

        public GameObject ContentsPrefab { get; private set; }

        public Action<GameObject> InitializePrefab { get; private set; }

        public void SetContents(GameObject contentsPrefab, Action<GameObject> initializePrefab)
        {
            ContentsPrefab = contentsPrefab;
            InitializePrefab = initializePrefab;
        }

        public void Open()
        {
            if (!_opened)
            {
                var contents = Instantiate(ContentsPrefab);
                contents.transform.position = transform.position;
                InitializePrefab?.Invoke(contents);

                Destroy(gameObject);
                _opened = true;
            }
        }
    }
}