﻿namespace Assets.Scripts.MonoBehaviour.Display
{
    using Assets.Scripts.Core;

    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;

    using Zenject;

    public class UIController : MonoBehaviour
    {
        public GameObject WinDialog;
        public GameObject LoseDialog;
        public GameObject StartDialog;
        public Button StartButton;
        public Text LevelNumberText;

        public Button QuitButton;

        public Button RetryButton;
        public Button NextLevelButton;

        private GameStateManager _gameStateManager;

        [Inject]
        public void Inject(GameStateManager gameStateManager)
        {
            _gameStateManager = gameStateManager;
        }

        private void Start()
        {
            LevelNumberText.text = LevelCounter.LevelNumber.ToString();

            _gameStateManager.OnWin += () => WinDialog.SetActive(true);
            _gameStateManager.OnLose += () => LoseDialog.SetActive(true);

            StartButton.onClick.AddListener(() =>
                {
                    _gameStateManager.Trigger(GameStateEvent.Start);
                    StartDialog.SetActive(false);
                });

            QuitButton.onClick.AddListener(Application.Quit);
            RetryButton.onClick.AddListener(
                () =>
                    {
                        SceneManager.LoadScene("Level");
                        LoseDialog.SetActive(false);
                    });
            NextLevelButton.onClick.AddListener(
                () =>
                    {
                        LevelCounter.LevelNumber++;
                        SceneManager.LoadScene("Level");
                        WinDialog.SetActive(false);
                    });
        }
    }
}
