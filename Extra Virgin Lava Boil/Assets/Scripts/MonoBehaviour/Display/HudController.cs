﻿namespace Assets.Scripts.MonoBehaviour.Display
{
    using Assets.Scripts.MonoBehaviour.Game;

    using UnityEngine;
    using UnityEngine.UI;

    public class HudController : MonoBehaviour
    {
        public Text TimerText;
        public Text SacrificesText;

        private ObjectiveController _objectiveController;

        // Use this for initialization
        private void Start()
        {
            _objectiveController = GetComponent<ObjectiveController>();
        }

        // Update is called once per frame
        private void Update()
        {
            TimerText.text = _objectiveController.GetTimeRemaining().ToString("###0");
            SacrificesText.text = _objectiveController.GetSacrificesRemaining().ToString("###0");
        }
    }
}
