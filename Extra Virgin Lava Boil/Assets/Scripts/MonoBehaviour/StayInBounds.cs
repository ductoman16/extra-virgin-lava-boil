﻿namespace Assets.Scripts.MonoBehaviour
{
    using UnityEngine;

    public class StayInBounds : MonoBehaviour
    {
        public Collider2D Boundary;

        private const float BoundaryOffset = .2f;

        private void FixedUpdate()
        {
            var bounds = Boundary.bounds;
            var clampedX = Mathf.Clamp(transform.position.x, bounds.min.x + BoundaryOffset, bounds.max.x - BoundaryOffset);
            var clampedY = Mathf.Clamp(transform.position.y, bounds.min.y + BoundaryOffset, bounds.max.y - BoundaryOffset);
            transform.position = new Vector2(clampedX, clampedY);
        }

        private bool IsInBounds()
        {
            var bounds = Boundary.bounds;
            return bounds.min.y < transform.position.y && transform.position.y < bounds.max.y
                && bounds.min.x < transform.position.x && transform.position.x < bounds.max.x;
        }
    }
}
