﻿namespace Assets.Scripts.MonoBehaviour
{
    using UnityEngine;

    public class FollowBehindTarget : MonoBehaviour
    {
        public float Speed = 1;
        public float FollowDistance = 1;

        private Rigidbody2D _rigidBody;

        public Transform Target { get; set; }

        private void Start()
        {
            _rigidBody = GetComponent<Rigidbody2D>();
        }

        //private void OnCollisionStay2D(Collision2D other)
        //{
        //    _rigidBody.velocity = new Vector2(_rigidBody.velocity.y, _rigidBody.velocity.x);
        //}

        private void Update()
        {
            if (Target != null)
            {
                Follow();
            }
        }

        private void Follow()
        {
            var distance = Target.position - transform.position;

            if (distance.magnitude > FollowDistance)
            {
                var normalized = distance.normalized;
                _rigidBody.velocity = new Vector2(normalized.x * Speed, normalized.y * Speed);
            }
        }
    }
}