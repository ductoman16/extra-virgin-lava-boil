﻿namespace Assets.Scripts.MonoBehaviour.Fireball
{
    using Assets.Scripts.Core;
    using Assets.Scripts.MonoBehaviour.Player;

    using UnityEngine;

    using Zenject;

    public class FireballSpawner : MonoBehaviour
    {
        public int FireballSpawningAreaWidth = 10;

        public GameObject FireballPrefab;
        public Collider2D FireballBoundary;

        public BoxCollider2D PlayerCollider;
        public PickUpBehavior PlayerPickUpBehavior;

        private float _spawnRate;

        private GameStateManager _gameStateManager;

        [Inject]
        public void Inject(GameStateManager gameStateManager, LevelConfig config)
        {
            _spawnRate = config.FireballSpawnRate;
            _gameStateManager = gameStateManager;
        }

        private void Update()
        {
            if (ShouldSpawn())
            {
                Spawn();
            }
        }

        private void Spawn()
        {
            var fireball = Instantiate(FireballPrefab, transform);
            fireball.transform.position = transform.position;

            var randomPosition = Random.Range(-FireballSpawningAreaWidth * 10 / 2, FireballSpawningAreaWidth * 10 / 2) / 10f;
            var fireballPosition = fireball.transform.position;
            fireball.transform.position = new Vector2(fireballPosition.x + randomPosition, fireballPosition.y);

            var hitPlayerBehavior = fireball.GetComponent<HitPlayer>();
            hitPlayerBehavior.Init(PlayerCollider, PlayerPickUpBehavior);

            var destroyOnLeaveBoundary = fireball.GetComponent<DestroyOnLeaveBoundary>();
            destroyOnLeaveBoundary.Boundary = FireballBoundary;
        }

        private bool ShouldSpawn()
        {
            if (_gameStateManager.GameState != GameState.Active)
            {
                return false;
            }

            var random = Random.Range(0, 100);
            return random < _spawnRate;
        }
    }
}
