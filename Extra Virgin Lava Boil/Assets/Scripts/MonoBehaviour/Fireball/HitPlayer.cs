﻿namespace Assets.Scripts.MonoBehaviour.Fireball
{
    using System.Linq;

    using Assets.Scripts.MonoBehaviour.Player;

    using UnityEngine;

    public class HitPlayer : MonoBehaviour
    {
        public BoxCollider2D PlayerCollider { get; private set; }

        public void Init(BoxCollider2D playerCollider, PickUpBehavior playerPickUpBehavior)
        {
            PlayerCollider = playerCollider;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (IsPlayer(other))
            {
                Hit();
            }
        }

        private void Hit()
        {
            var collectChickensBehavior = PlayerCollider.GetComponent<CollectChickens>();
            if (collectChickensBehavior.Chickens.Any())
            {
                collectChickensBehavior.DropChicken();
            }
            else
            {
                var playerPickUpBehavior = PlayerCollider.GetComponent<PickUpBehavior>();
                playerPickUpBehavior.Drop();
            }
        }

        private bool IsPlayer(Collider2D other)
        {
            return ReferenceEquals(PlayerCollider, other);
        }
    }
}
