﻿namespace Assets.Scripts.MonoBehaviour.Fireball
{
    using UnityEngine;

    public class DestroyOnLeaveBoundary : MonoBehaviour
    {
        public Collider2D Boundary { get; set; }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (IsBoundary(other))
            {
                Destroy(gameObject);
            }
        }

        private bool IsBoundary(Collider2D other)
        {
            return ReferenceEquals(Boundary, other);
        }
    }
}
