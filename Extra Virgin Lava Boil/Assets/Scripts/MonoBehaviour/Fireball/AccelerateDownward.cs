﻿namespace Assets.Scripts.MonoBehaviour.Fireball
{
    using UnityEngine;

    public class AccelerateDownward : MonoBehaviour
    {
        public float Velocity;

        // Use this for initialization
        private void Start()
        {
            var rigidBody = GetComponent<Rigidbody2D>();
            rigidBody.velocity = new Vector2(0, -Velocity);
        }
    }
}
