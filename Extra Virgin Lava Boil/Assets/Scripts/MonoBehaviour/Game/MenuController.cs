﻿namespace Assets.Scripts.MonoBehaviour.Game
{
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;

    public class MenuController : MonoBehaviour
    {
        public Button StartButton;
        public Button QuitButton;

        // Use this for initialization
        private void Start()
        {
            StartButton.onClick.AddListener(() =>
                {
                    SceneManager.LoadScene("Level");
                });
            QuitButton.onClick.AddListener(Application.Quit);
        }
    }
}
