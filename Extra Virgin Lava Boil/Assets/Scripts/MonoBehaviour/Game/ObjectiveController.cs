﻿namespace Assets.Scripts.MonoBehaviour.Game
{
    using Assets.Scripts.Core;

    using UnityEngine;

    using Zenject;

    public class ObjectiveController : MonoBehaviour
    {
        private GameStateManager _gameStateManager;
        private int _sacrificesRequired;
        private int _timerSeconds;

        private float _startTime;

        public int SacrificesMade { get; set; } = 0;

        [Inject]
        public void Inject(GameStateManager gameStateManager, LevelConfig config)
        {
            _sacrificesRequired = config.SacrificesRequired;
            _timerSeconds = config.TimerSeconds;
            _gameStateManager = gameStateManager;
        }

        public int GetSacrificesRemaining()
        {
            return _sacrificesRequired - SacrificesMade;
        }

        public float GetTimeRemaining()
        {
            var endTime = _startTime + _timerSeconds;
            return endTime - Time.time;
        }

        private void Start()
        {
            StopTime();

            _gameStateManager.OnWin += StopTime;
            _gameStateManager.OnLose += StopTime;
            _gameStateManager.OnTriggered(GameStateEvent.Start, StartTimer);
        }

        private void Update()
        {
            if (SacrificesMade >= _sacrificesRequired)
            {
                _gameStateManager.Trigger(GameStateEvent.Win, true);
            }

            if (TimerElapsed())
            {
                _gameStateManager.Trigger(GameStateEvent.Lose, true);
            }
        }

        private void StopTime()
        {
            Time.timeScale = 0;
        }

        private void StartTimer()
        {
            _startTime = Time.time;
            Time.timeScale = 1;
        }

        private bool TimerElapsed()
        {
            return GetTimeRemaining() <= 0;
        }
    }
}
